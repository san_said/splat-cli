
# SPLAT

## Quick Guide

- splat show <file-name>
	- show the contents of your splat file.

- splat install [-f <file-name>]
	- install all applications in a splat file
	- (optional) -f, enter the name of the splat file you want to install

- splat new [-p <path-name>] [-f <file-name>]
	- creates new splat file in current working directory.
	- (optional) -p, enter *absolute* path name of where you want to add the new splat file
	- (optional) -f, enter the name of the name of your splat file

- splat add <package-name> [-t <tagname>] [-d <description>] [-f <filename>]
	- add a package name to the splat file
	- (optional) -t, tag name of the splat entry for easy identification
	- (optional) -d, enter a brief description for the package
	- (optional) -f, specify filename to add package name to

- splat config [-p <path-name>] [-cfn <file-name1> <file-name2>] [-r <file-name>]
	- modify your splat files. If no arguments added, will print the existing path name of your splat files and the name of all existing splat files.
	- (optional) -p, change where splat is looking for your splat files 
	- (optional) -cfn, change the file name of any of your splat files
	- (optional) -r, delete a splat file

- splat remove <tag-name> [-f <file-name>]
	- remove an entry from a splat file by its tag name
	- (optional) -f, specify the filename to remove the splat entry frome

## Using SPLAT

### Adding an entry to your Splat file

To add an entry to your Splat file, you can type ```splat add``` followed by the name of the package you want to add to your Splat file. Packages must be recognised by the apt-get and snap package managers. Example:

```splat add docker```

If you are not sure of the name of your package, you can start typing the first few letters of the package name and Splat will output the closest matches it could find. All you have to do now is pick the ID of the one you were looking for and add a custom message, if you want.

### Changing the path of your Splat file

Adding an entry for the first time creates a Splat file in the same path as your current working directory. Splat will reference this path in the future whenever you run Splat from anywhere else.

To change the path of you current Splat file, modify using:

```splat -p <path-name>```

You can use absolute or relative paths.

**Warning:** You can only create one instance of a Splat file. Future releases will allow you to create multiple Splat files that can be referenced and amended as needed. 

### Adding a tag name to your Splat entry

To add a tag name to your Splat entry, type:

```splat add docker -t <tag-name>```

Tag names must be unique and make it easy to identify an entry, especially when you need to remove or edit an entry.

If you do not entry a tag name, Splat will default by using the package name as the tag name.

### Adding custom descriptions to your Splat entries

You can add custom, non-unique descriptions  against your Splat entries. This is useful to describe what a package does in brief if you can't remember why you added it to your Splat file.

To add a custom description, use:

```splat add docker -d "This is my custom description"```

If no custom description is used, Splat will store the default  description in the meta-data of your package (i.e. the outcomes of ```sudo apt-cache show <app-name>```).

### Editing the custom description of your Splat entries

If you would like to edit the custom description against an existing Splat entry, use:

```splat edit <tag-name> -d "I have edited this description"```

You can also use the name of the package if you are unsure of the tag name.

If there are two Splat entries with similar names, Splat will provide a list of these entries and you must pick from either in order to edit.

### Removing a Splat entry

To remove an existing Splat entry, either reference its tag name or package name (if unique):

```splat remove <tagname or packagename>```

### See the status of your current Splat file

To see the status of your current Splat file, you can just type ```splat``` to output all the entries and its comments.

### Adding newly installed apps to your Splat file

To add a newly installed app to your SPLAT file, just follow it up with a pipe:

```supo apt-get docker | splat add```

This also applies to snap applications:

```sudo snap docker | splat add```

## References

[1] Building simple CLI in Python:  https://stormpath.com/blog/building-simple-cli-interfaces-in-python
