"""Imports"""
import re as r
import os as o
import config as c

### SUBSIDIARY FUNCTIONS ###

"""Counts number of splat files in a directory - non-recursive"""
def list_splat_files(pathname):
	splat_format = r.compile("^.+/.splat$") 
	return [d for d in o.listdir(pathname) if splat_format.match(d)]

def retrieve_default_pathname():


### MAIN FUNCTION ###
def add(pathname, filename, pkg, tagname, desc):
	"""If no filename specified, check if there is a unique splat file that can be used"""
	if not filename:
		splat_files = list_splat_files(pathname)
		if len(splat_files)==1:
			filename = splat_files[0]
		else:
			print("""
				We have located multiple splat files. 

				Please specify the name of your splat file from the list below
				with -f followed by the splat file name.

				{}
				""".format(splat_files))
	
	# TODO: IF NO PATH NAME SPECIFIED, RETRIEVE PATH NAME FROM SETTINGS.JSON
	# TODO: FIND A WAY TO CENTRALISE SETTINGS AND CONFIG LOCATION SO THAT THEY CAN ALWAYS BE LOCATED NO MATTER WHERE THEY MOVE
	if not pathname:
		pathname = c.SETTINGS_PATH






