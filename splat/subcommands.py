"""
This module contains all the functions invoked by subcommands.
The aim is to maintain one function per subcommand.
"""

"""Subcommand for adding an entry to a Splat file"""
def add(pkg_name,tag_name,desc):
	print(
	"""
	Package name: {},
	Tag name: {},
	Description: {}
	""".format(pkg_name, tag_name, desc))

"""Subcommand for editing an entry in a Splat file"""
def edit(pkg_name,tag_name,desc):
	return none

"""Subcommand for removing an entry from a Splat file"""
def remove(pkg_name):
	return none

"""Subcommand for installing all packages in Splat file"""
def install(splat_file):
	return none

"""Subcommand for creating a new Splat file"""
def new(filename, pathname):
	# moved to new.py