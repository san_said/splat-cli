"""Imports"""
import os as o
import datetime as dt

### SUBSIDIARY FUNCTIONS ###

"""Generates a unique filename based on current datetime"""
def generate_filename():
	f = "".join(str(dt.datetime.now()).split(" "))
	f = "".join(f.split("-"))
	f = "".join(f.split(":"))
	f = "_".join(f.split("."))

	return f

### MAIN FUNCTION ###
def new(filename, pathname):
	"""If no file name specified, generate one based on current datetime"""
	if not filename:
		filename = generate_filename()

	"""Create file name with extension"""
	splat_filename = filename  + ".splat"

	"""If no path name specified, use current working directory; else validate directory name passed"""
	if not pathname:
		pathname = o.getcwd()
	elif not os.path.isdir(pathname):
		print("Please enter a valid path. The path you have provided does not exist.")		

	"""Check that file does not already exist"""
	if os.path.isfile(pathname + "/" + splat_filename):
		print("File name already exists! Exiting program to prevent overwriting.")

	"""Create splat file"""
	fSplat = open(pathname + "/" + splat_filename, "w+")
	fSplat.close()
