#!/usr/bin/env python

"""
SPLAT

Usage:
    splat add
    splat edit
    splat remove
    splat -h | --help
    splat -v | --version

Options:
    -h --help			Show this screen.
    -v --version		Show current version number.
    -d --description	Modify description to the in-focus splat entry.
    -t --tagname		Modify the tagname against the in-focus splat entry
"""

"""Imports"""
import argparse as a
import json as j
import os as o

"""Main CLI entry point."""
if __name__ == "__main__":
	"""Setting up main parser"""
	SPLAT = a.ArgumentParser(
			prog="Splat",
			description="Create a list of packages that you can automatically install. Useful for quickly installing all of your essential applications when setting up a new work environment."
		)

	"""Adding subcommands"""
	SPLAT.add_argument("new",
				type=str,
				help="Create a new splat file. Expects no further arguments.")

	SPLAT.add_argument("install",
				type=str,
				help="Install all apps in a splat file.")

	SPLAT.add_argument("add",
				type=str,
				help="Subcommand to add entries into splat file.")

	SPLAT.add_argument("edit",
				type=str,
				help="Subcommand to edit an entry in a splat file. Expects package name.")

	SPLAT.add_argument("remove",
				type=str,
				help="Subcommand to remove an entry from a splat file. Expects package name.")

	SPLAT.add_argument("config",
				type=str,
				help="Subcommand to modify the configuration of your splat files. If no arguments or passed, prints the current configuration.")

	"""Adding arguments"""
	SPLAT.add_argument("-p", "--path",
	            type=str,
	            help="Define the path of your Splat file. Your current working directory will be used as default if no path is specified.",
	            metavar="Path")

	SPLAT.add_argument("-d", "--description",
				type=str,
				help="Change or add a description to your package name. This is useful if you want to remember the reason why you need this package. Default packaage description will be used by default.",
				metavar="Description")

	SPLAT.add_argument("-t", "--tagname",
				type=str,
				help="Change or add the tag name of a splat entry. This is useful if you want to retrieve or edit a package with a more memorable name.",
				metavar="Tagname")

	args, unknown_args = SPLAT.parse_known_args()